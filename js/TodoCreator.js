import { html, render, Component } from 'https://unpkg.com/htm/preact/standalone.module.js'

class TodoCreator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayStyle: 'none',
            inputValue: ''
        };
    }

    render() {
        return html`
            <button id="add_button" onclick="${this.enableInput.bind(this)}">Add</button>
            <div id="dialog" style="display: ${this.state.displayStyle}">
                <input id="task_text" value=${this.state.inputValue} oninput="${this.handleInput.bind(this)}"></input>
                <button onclick="${this.handleAdd.bind(this)}">Add</button>
                <button onclick="${this.disableInput.bind(this)}">Cancel</button>
            </div>`;
    }

    enableInput() {
        this.setState({displayStyle: 'block'});
    }

    disableInput() {
        this.setState({displayStyle: 'none'});
    }

    handleInput(e) {
        this.setState({inputValue: e.target.value});
    }

    handleAdd() {
        this.setState({inputValue: '', displayStyle: 'none'});
        this.props.add(this.state.inputValue);
    }   
}

export default TodoCreator;