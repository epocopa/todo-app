if (!self.define) {
  const e = (e) => {
      "require" !== e && (e += ".js");
      let s = Promise.resolve();
      return (
        r[e] ||
          (s = new Promise(async (s) => {
            if ("document" in self) {
              const r = document.createElement("script");
              (r.src = e), document.head.appendChild(r), (r.onload = s);
            } else importScripts(e), s();
          })),
        s.then(() => {
          if (!r[e]) throw new Error(`Module ${e} didn’t register its module`);
          return r[e];
        })
      );
    },
    s = (s, r) => {
      Promise.all(s.map(e)).then((e) => r(1 === e.length ? e[0] : e));
    },
    r = { require: Promise.resolve(s) };
  self.define = (s, i, o) => {
    r[s] ||
      (r[s] = Promise.resolve().then(() => {
        let r = {};
        const c = { uri: location.origin + s.slice(1) };
        return Promise.all(
          i.map((s) => {
            switch (s) {
              case "exports":
                return r;
              case "module":
                return c;
              default:
                return e(s);
            }
          })
        ).then((e) => {
          const s = o(...e);
          return r.default || (r.default = s), r;
        });
      }));
  };
}
define("./sw.js", ["./workbox-1bbb3e0e"], function (e) {
  "use strict";
  self.addEventListener("message", (e) => {
    e.data && "SKIP_WAITING" === e.data.type && self.skipWaiting();
  }),
    e.precacheAndRoute(
      [
        { url: "css/main.css", revision: "a9805bdf91649564208423df1b122c0e" },
        {
          url: "css/skeleton.css",
          revision: "cd542f65c9e43abc5ea195c9ddae1bb9",
        },
        {
          url: "images/apple-touch-icon.png",
          revision: "5d089511e9a254b5f1355b55eb0afe8a",
        },
        {
          url: "images/icon-192x192.png",
          revision: "1ad8f93c616aa7478e191aaf34d9ca37",
        },
        {
          url: "images/icon-512x512.png",
          revision: "537d3d0ce6243454a359b4e5ce126caf",
        },
        { url: "index.html", revision: "bfb8376d2edca6dd127165a4d225bd78" },
        { url: "js/App.js", revision: "1c8f6df51d893399afe188e5d9fa60ba" },
        {
          url: "js/TodoCreator.js",
          revision: "d5fea9599465ac06544fa3c9d167bc00",
        },
        { url: "js/TodoList.js", revision: "04b9a670a18044d507200a5955bf9f42" },
        { url: "manifest.json", revision: "ba042ffda9a6021280b12623e4ac4c3b" },
        {
          url: "package-lock.json",
          revision: "d0b9e879440e91c4e66e75f8fb2e1e11",
        },
        { url: "package.json", revision: "8fd2960cf5b948595b4d4cafccfb0142" },
      ],
      {}
    );
});
//# sourceMappingURL=sw.js.map
